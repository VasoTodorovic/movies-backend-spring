# Movies Backend Spring

Backend-a demo project for a film review using Spring Boot that consume [Mongo DB Atlas](https://cloud.mongodb.com/v2/63ee24cac33f924bd71a8f8c#/clusters) cloud database that I create myself
Frontend for this app [React](https://gitlab.com/VasoTodorovic/movie-frontend-react) 

what i learned:
- work with Mongo Db
- Building Mongo databe in Mongo DB Atlas cloud (data lake)
- connection from Spring Boot to a database in the cloud (Mongo DB Atlas) that I create myself
- managing Mongo db from Spring Boot using MongoRepository, MongoTemplate

technology:
- Spring Boot
- Mongo DB Atlas (cloud database)
- Mongo DB
- Spring Data Mongo
- Lombok
- Maven


